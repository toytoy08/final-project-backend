package com.finalproject.dv.shopguide.util

import com.finalproject.dv.shopguide.entity.dto.UserDataDto
import com.finalproject.dv.shopguide.entity.Review
import com.finalproject.dv.shopguide.entity.TripTour
import com.finalproject.dv.shopguide.entity.TripTourData
import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.entity.dto.ReviewDto
import com.finalproject.dv.shopguide.entity.dto.TripTourDataDto
import com.finalproject.dv.shopguide.entity.dto.TripTourDto
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "shop")
interface MapperShop {
    companion object {
        val SHOPINSTANCE = Mappers.getMapper(MapperShop::class.java)
    }

    fun mapUserData(userData: UserData): UserDataDto
    fun mapUserDataList(userData: List<UserData>): List<UserDataDto>
    @InheritInverseConfiguration
    fun mapUserDataDto(userDataDto: UserDataDto): UserData

    fun mapReview(review: Review): ReviewDto
    fun mapReviewList(review: List<Review>): List<ReviewDto>
    @InheritInverseConfiguration
    fun mapReviewDto(reviewDto: ReviewDto): Review

    fun mapTripTour(tripTour: TripTour): TripTourDto
    fun mapTripTourList(trip: List<TripTour>): List<TripTourDto>
    @InheritInverseConfiguration
    fun mapTripTourDto(tripTourDto: TripTourDto): TripTour

    fun mapTripTourData(tripTourData: TripTourData): TripTourDataDto
    fun mapTripTourDataList(tripTourData: List<TripTourData>): List<TripTourDataDto>
    @InheritInverseConfiguration
    fun mapTripTourDataDto(tripTourDataDto: TripTourDataDto): TripTourData
}