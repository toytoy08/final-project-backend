package com.finalproject.dv.shopguide.controller

import com.finalproject.dv.shopguide.entity.Review
import com.finalproject.dv.shopguide.entity.dto.ReviewDto
import com.finalproject.dv.shopguide.service.ReviewService
import com.finalproject.dv.shopguide.util.MapperShop
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ReviewController {
    @Autowired
    lateinit var reviewService: ReviewService

    @GetMapping("/review")
    fun getReview(): ResponseEntity<Any> {
        val review = reviewService.getReviewData()
        return ResponseEntity.ok(MapperShop.SHOPINSTANCE.mapReviewList(review))
    }

    @GetMapping("/review/{reviewid}")
    fun getReviewById(@PathVariable("reviewid") reviewid: Long?): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapReview(reviewService.getReviewById(reviewid))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND THIS REVIEW")
    }

    @PostMapping("/review/{userid}")
    fun postReview(@PathVariable("userid") userid: Long?,
                   @RequestBody review: Review): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapReview(reviewService.save(userid, review))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("DUPLICATE REVIEW")
    }

    @PutMapping("/review/editreview/{reviewid}")
    fun putUserEmail(@PathVariable("reviewid") reviewid: Long?,
                     @RequestBody review: Review): ResponseEntity<Any> {
        review.id = reviewid
        var output = MapperShop.SHOPINSTANCE.mapReview(reviewService.edit(reviewid, review))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("SOMETHING WRONG THIS REVIEW")
    }

    @DeleteMapping("/review/deletereview/{reviewid}")
    fun deleteReviewById(@PathVariable("reviewid") reviewid: Long?): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapReview(reviewService.delete(reviewid))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("DELETED")
    }
}