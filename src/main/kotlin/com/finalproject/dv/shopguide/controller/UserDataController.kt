package com.finalproject.dv.shopguide.controller

import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.entity.dto.UserDataDto
import com.finalproject.dv.shopguide.service.UserDataService
import com.finalproject.dv.shopguide.util.MapperShop
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserDataController {
    @Autowired
    lateinit var userDataService: UserDataService

    @GetMapping("/user")
    fun getUserData(): ResponseEntity<Any> {
        val userdata = userDataService.getUserData()
        return ResponseEntity.ok(MapperShop.SHOPINSTANCE.mapUserDataList(userdata))
    }

    @GetMapping("/user/{useremail}")
    fun getUserEmail(@PathVariable("useremail") useremail: String?): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapUserData(userDataService.getUseremail(useremail))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND ANYTHING")
    }

    @PostMapping("/user/register")
    fun postUser(@RequestBody userData: UserDataDto): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapUserData(userDataService.save(userData))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.IM_USED).body("THIS ID IS ALREADY USED")
    }

    @PutMapping("/user/editprofile/{userid}")
    fun putUserEmail(@PathVariable("userid") userid: Long?,
                     @RequestBody userDataDto: UserDataDto): ResponseEntity<Any> {
        userDataDto.id = userid
        var output = MapperShop.SHOPINSTANCE.mapUserData(userDataService.save(userDataDto))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND THIS USER")
    }
}