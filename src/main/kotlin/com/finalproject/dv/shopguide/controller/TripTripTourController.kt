package com.finalproject.dv.shopguide.controller

import com.finalproject.dv.shopguide.entity.TripTour
import com.finalproject.dv.shopguide.entity.TripTourData
import com.finalproject.dv.shopguide.service.TripTourDataService
import com.finalproject.dv.shopguide.service.TripTourService
import com.finalproject.dv.shopguide.util.MapperShop
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class TripTripTourController {
    @Autowired
    lateinit var tripTourService: TripTourService

    @Autowired
    lateinit var tripTourDataService: TripTourDataService

    @GetMapping("/triptour")
    fun getTripTour(): ResponseEntity<Any> {
        val triptour = tripTourService.getTriptour()
        return ResponseEntity.ok(MapperShop.SHOPINSTANCE.mapTripTourList(triptour))
    }

    @GetMapping("/triptour/data")
    fun getTripTourData(): ResponseEntity<Any> {
        val triptourdata = tripTourDataService.getTripTourData()
        return ResponseEntity.ok(MapperShop.SHOPINSTANCE.mapTripTourDataList(triptourdata))
    }

    @GetMapping("/triptour/{tripid}")
    fun getTripTourById(@PathVariable("tripid") tripid: Long?): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapTripTour(tripTourService.getTripTourById(tripid))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND THIS TRIP")
    }

    @GetMapping("/triptour/data/{tripid}")
    fun getTripTourDataById(@PathVariable("tripid") tripid: Long?): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapTripTourData(tripTourDataService.getTripTourDataById(tripid))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND THIS TRIP")
    }

    @PostMapping("/triptour/trip/{userid}")
    fun postTripTourByUser(@PathVariable("userid") userid: Long?,
                           @RequestBody tripTour: TripTour): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapTripTour(tripTourService.save(userid, tripTour))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("THIS TRIP IS ALREADY USED")
    }

    @PostMapping("/triptour/data/trip/{tripid}")
    fun postTripTourDataByTripTourId(@PathVariable("tripid") tripid: Long?,
                                     @RequestBody tripTourData: TripTourData): ResponseEntity<Any> {
        var output = MapperShop.SHOPINSTANCE.mapTripTourData(tripTourDataService.save(tripid, tripTourData))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("THIS TRIPDATA IS ALREADY USED")
    }

    @PutMapping("/triptour/data/edittrip/{tripid}")
    fun putTripTourData(@PathVariable("tripid") tripid: Long?,
                        @RequestBody tripTourData: ArrayList<TripTourData>): ResponseEntity<Any> {
        val output = tripTourDataService.edit(tripid, tripTourData)
        val outputDto = MapperShop.SHOPINSTANCE.mapTripTourDataList(output)
        var outputSortOrder = outputDto.sortedWith(compareBy({ it.triporder }))
        outputSortOrder?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND")
    }

    @DeleteMapping("/triptour/delete/{tripid}")
    fun deleteTripTourById(@PathVariable ("tripid") tripid: Long?) : ResponseEntity<Any>{
        val output = tripTourService.remove(tripid)
        val outputDto = MapperShop.SHOPINSTANCE.mapTripTour(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND")
    }

    @DeleteMapping("/triptour/data/delete/{tripid}")
    fun deleteTripTourDataById(@PathVariable ("tripid") tripid: Long?) : ResponseEntity<Any>{
        val output = tripTourDataService.remove(tripid)
        val outputDto = MapperShop.SHOPINSTANCE.mapTripTourData(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND")
    }
}