package com.finalproject.dv.shopguide.config

import com.finalproject.dv.shopguide.entity.Review
import com.finalproject.dv.shopguide.entity.TripTour
import com.finalproject.dv.shopguide.entity.TripTourData
import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.repository.ReviewRepository
import com.finalproject.dv.shopguide.repository.TripTourDataRepository
import com.finalproject.dv.shopguide.repository.TripTourRepository
import com.finalproject.dv.shopguide.repository.UserDataRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class DataLoaderShop : ApplicationRunner {
    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var reviewRepository: ReviewRepository
    @Autowired
    lateinit var tripTourRepository: TripTourRepository
    @Autowired
    lateinit var tripTourDataRepository: TripTourDataRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {

        var user1 = userDataRepository.save(UserData(
                "test@gmail.com",
                "asdf",
                "test",
                "this"
        ))

        var user2 = userDataRepository.save(UserData(
                "abc@gmail.com",
                "aaaa",
                "my",
                "lastname"
        ))

        var review1 = reviewRepository.save(Review(
                "4",
                3,
                "Very Nice"
        ))

        var review2 = reviewRepository.save(Review(
                "4",
                4,
                "Very Very Nice"
        ))

        var review3 = reviewRepository.save(Review(
                "5",
                1,
                "So bad"
        ))

        var triptour1 = tripTourRepository.save(TripTour(
                "Happy Trip"
        ))

        var triptour2 = tripTourRepository.save(TripTour(
                "Sun Trip"
        ))

        var tripdata1 = tripTourDataRepository.save(TripTourData(
                "1",
                1
        ))

        var tripdata2 = tripTourDataRepository.save(TripTourData(
                "2",
                2
        ))

        review1.userData = user1
//        review2.userData = user1
        review2.userData = user2
        review3.userData = user1

        user1.review.add(review1)
        user1.review.add(review3)
        user2.review.add(review2)

        triptour1.userData = user1
//        triptour1.tripTourData.add(tripdata1)

        triptour2.userData = user2
//        triptour2.tripTourData.add(tripdata2)

        tripdata1.tripTour = triptour1
        tripdata2.tripTour = triptour2
    }
}