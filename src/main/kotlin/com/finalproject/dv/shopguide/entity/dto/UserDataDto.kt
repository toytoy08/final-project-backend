package com.finalproject.dv.shopguide.entity.dto

class UserDataDto(var id: Long? = null,
                  var email: String? = null,
                  var firstname: String? = null,
                  var lastname: String? = null,
                  var image: String? = "https://i.imgur.com/xoFYkyq.png")