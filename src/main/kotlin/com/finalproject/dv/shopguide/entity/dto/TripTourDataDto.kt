package com.finalproject.dv.shopguide.entity.dto

data class TripTourDataDto(
        var id: Long? = null,
        var shopId: String? = null,
        var triporder: Int? = null,
        var tripTour: TripTourDto? = null
)