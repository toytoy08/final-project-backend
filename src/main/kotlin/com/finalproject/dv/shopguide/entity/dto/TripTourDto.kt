package com.finalproject.dv.shopguide.entity.dto

data class TripTourDto(
        var id: Long? = null,
        var tripName: String? = null,
        var userData: UserDataDto? = null
)