package com.finalproject.dv.shopguide.entity.dto

data class ReviewDto(
        var id: Long? = null,
        var shopid: String? = null,
        var rate: Int? = null,
        var comment: String? = null,
        var userData: UserDataDto? = null
)