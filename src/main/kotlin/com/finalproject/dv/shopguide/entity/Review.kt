package com.finalproject.dv.shopguide.entity

import javax.persistence.*

@Entity
data class Review(var shopid: String? = null,
                  var rate: Int? = null,
                  var comment: String? = null,
                  var isDeleted: Boolean = false) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var userData: UserData? = null
//
//    constructor(
//            shopid: String?,
//            rate: Int,
//            comment: String?,
//            userData: UserData) :
//            this(shopid, rate, comment) {
//        this.userData = userData
//    }
}