package com.finalproject.dv.shopguide.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class TripTourData(
        var shopId: String? = null,
        var triporder: Int? = null,
        var isDeleted: Boolean = false
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var tripTour: TripTour? = null
}