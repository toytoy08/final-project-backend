package com.finalproject.dv.shopguide.entity

import javax.persistence.*

@Entity
data class TripTour(
        var tripName: String? = null,
        var isDeleted: Boolean = false
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var userData: UserData? = null
}