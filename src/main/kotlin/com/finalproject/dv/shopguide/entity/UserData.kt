package com.finalproject.dv.shopguide.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class UserData(
        var email: String? = null,
        var password: String? = null,
        var firstname: String? = null,
        var lastname: String? = null,
        var image: String? = "https://i.imgur.com/xoFYkyq.png"
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var review = mutableListOf<Review>()
}