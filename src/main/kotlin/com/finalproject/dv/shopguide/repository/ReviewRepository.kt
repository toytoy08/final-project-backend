package com.finalproject.dv.shopguide.repository

import com.finalproject.dv.shopguide.entity.Review
import org.springframework.data.repository.CrudRepository

interface ReviewRepository : CrudRepository<Review, Long> {
    fun findByIdAndIsDeletedIsFalse(id: Long?): Review
    fun findByIsDeletedIsFalse(): List<Review>
}