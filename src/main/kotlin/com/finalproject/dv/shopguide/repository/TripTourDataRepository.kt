package com.finalproject.dv.shopguide.repository

import com.finalproject.dv.shopguide.entity.TripTourData
import org.springframework.data.repository.CrudRepository

interface TripTourDataRepository : CrudRepository<TripTourData, Long> {
    fun findByIsDeletedIsFalse(): List<TripTourData>
    fun findByIdAndIsDeletedIsFalse(tripid: Long?): TripTourData
    fun findTripTourDataByTripTourIdAndIsDeletedIsFalse(tripId: Long?): List<TripTourData>

}