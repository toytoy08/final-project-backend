package com.finalproject.dv.shopguide.repository

import com.finalproject.dv.shopguide.entity.TripTour
import org.springframework.data.repository.CrudRepository

interface TripTourRepository : CrudRepository<TripTour, Long> {
    fun findByIsDeletedIsFalse(): List<TripTour>
    fun findByIdAndIsDeletedIsFalse(tripid: Long?): TripTour

}