package com.finalproject.dv.shopguide.repository

import com.finalproject.dv.shopguide.entity.UserData
import org.springframework.data.repository.CrudRepository

interface UserDataRepository : CrudRepository<UserData, Long> {
    fun findByEmailContainingIgnoreCase(useremail: String?): UserData
    fun findUserDataById(id: Long?): UserData
}