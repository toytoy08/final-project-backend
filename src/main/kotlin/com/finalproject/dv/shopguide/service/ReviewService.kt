package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.entity.Review

interface ReviewService {
    fun getReviewData(): List<Review>
    fun getReviewById(reviewid: Long?): Review
    fun save(userid: Long?, review: Review): Review
    fun edit(reviewid: Long?, review: Review): Review
    fun delete(reviewid: Long?): Review

}