package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.entity.dto.UserDataDto

interface UserDataService {
    fun getUserData(): List<UserData>
    fun getUseremail(useremail: String?): UserData
    fun save(userData: UserDataDto): UserData
}