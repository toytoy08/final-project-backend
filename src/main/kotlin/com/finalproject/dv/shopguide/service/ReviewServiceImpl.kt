package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.dao.ReviewDao
import com.finalproject.dv.shopguide.dao.UserDataDao
import com.finalproject.dv.shopguide.entity.Review
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ReviewServiceImpl : ReviewService {
    @Transactional
    override fun delete(reviewid: Long?): Review {
        val review = reviewDao.getReviewById(reviewid)
        review?.isDeleted = true
        return review
    }

    @Transactional
    override fun edit(reviewid: Long?, review: Review): Review {
        val newReviewData = reviewDao.getReviewById(reviewid)
        val reviewData = Review()
        reviewData.id = reviewid
        reviewData.userData = newReviewData.userData
        reviewData.comment = review.comment
        reviewData.rate = review.rate
        reviewData.shopid = review.shopid
        return reviewDao.save(reviewData)
    }

    @Transactional
    override fun save(userid: Long?, review: Review): Review {
        val userData = userDataDao.getUserByID(userid)
        val ReviewData = Review()
        ReviewData.userData = userData
        ReviewData.comment = review.comment
        ReviewData.rate = review.rate
        ReviewData.shopid = review.shopid
        return reviewDao.save(ReviewData)
    }

    override fun getReviewById(reviewid: Long?): Review {
        return reviewDao.getReviewById(reviewid)
    }

    override fun getReviewData(): List<Review> {
        return reviewDao.getReview()
    }

    @Autowired
    lateinit var reviewDao: ReviewDao
    @Autowired
    lateinit var userDataDao: UserDataDao
}