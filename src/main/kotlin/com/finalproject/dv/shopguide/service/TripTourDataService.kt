package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.entity.TripTourData

interface TripTourDataService {
    fun save(tripid: Long?, tripTourData: TripTourData): TripTourData
    fun getTripTourData(): List<TripTourData>
    fun getTripTourDataById(tripid: Long?): TripTourData
    fun edit(tripid: Long?, tripTourData: ArrayList<TripTourData>): List<TripTourData>
    fun remove(tripid: Long?): TripTourData

}