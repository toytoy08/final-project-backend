package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.dao.TripTourDao
import com.finalproject.dv.shopguide.dao.TripTourDataDao
import com.finalproject.dv.shopguide.entity.TripTourData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.RuntimeException

@Service
class TripTourDataServiceImpl : TripTourDataService {
    @Transactional
    override fun remove(tripid: Long?): TripTourData {
        val tripTourData = tripTourDataDao.getTripTourDataById(tripid)
        tripTourData?.isDeleted = true
        return tripTourData
    }

    override fun edit(tripid: Long?, tripTourData: ArrayList<TripTourData>): List<TripTourData> {
        val tripTour = tripTourDao.getTripTourById(tripid)
        try {
            for (item in tripTourData) {
                val oldTripTourData = tripTourDataDao.getTripTourDataById(item.id)
                val currentTripTour = TripTourData()
                currentTripTour.id = item.id
                currentTripTour.shopId = oldTripTourData.shopId
                currentTripTour.triporder = item.triporder
                currentTripTour.tripTour = tripTour
                tripTourDataDao.save(currentTripTour)
            }
            return tripTourDataDao.getTripTourDataByTripTourId(tripid)
        } catch (e: RuntimeException) {
            return emptyList()
        }
    }

    override fun getTripTourDataById(tripid: Long?): TripTourData {
        return tripTourDataDao.getTripTourDataById(tripid)
    }

    override fun save(tripid: Long?, tripTourData: TripTourData): TripTourData {
        val triptour = tripTourDao.getTripTourById(tripid)
        val addtriptour = TripTourData()
        addtriptour.shopId = tripTourData.shopId
        addtriptour.triporder = tripTourData.triporder
        addtriptour.tripTour = triptour
        return tripTourDataDao.save(addtriptour)
    }

    override fun getTripTourData(): List<TripTourData> {
        return tripTourDataDao.getTripTourData()
    }

    @Autowired
    lateinit var tripTourDataDao: TripTourDataDao

    @Autowired
    lateinit var tripTourDao: TripTourDao
}