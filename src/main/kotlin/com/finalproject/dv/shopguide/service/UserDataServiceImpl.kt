package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.dao.UserDataDao
import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.entity.dto.UserDataDto
import com.finalproject.dv.shopguide.util.MapperShop
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserDataServiceImpl : UserDataService {

    @Transactional
    override fun save(userData: UserDataDto): UserData {
        val user = MapperShop.SHOPINSTANCE.mapUserDataDto(userData)
        return userDataDao.save(user)
    }

    override fun getUseremail(useremail: String?): UserData {
        return userDataDao.getUserEmail(useremail)
    }

    override fun getUserData(): List<UserData> {
        return userDataDao.getUserData()
    }

    @Autowired
    lateinit var userDataDao: UserDataDao
}