package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.entity.TripTour

interface TripTourService {
    fun getTriptour(): List<TripTour>
    fun getTripTourById(tripid: Long?): TripTour
    fun save(userid: Long?, tripTour: TripTour): TripTour
    fun remove(tripid: Long?): TripTour
}