package com.finalproject.dv.shopguide.service

import com.finalproject.dv.shopguide.dao.TripTourDao
import com.finalproject.dv.shopguide.dao.TripTourDataDao
import com.finalproject.dv.shopguide.dao.UserDataDao
import com.finalproject.dv.shopguide.entity.TripTour
import com.finalproject.dv.shopguide.entity.TripTourData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class TripTourServiceImpl : TripTourService {
    @Transactional
    override fun remove(tripid: Long?): TripTour {
        val tripTour = tripTourDao.getTripTourById(tripid)
        tripTour?.isDeleted = true
        var tripTourData = tripTourDataDao.getTripTourDataByTripTourId(tripid)
        for (item in tripTourData) {
            item?.isDeleted = true
        }
        return tripTour
    }

    override fun save(userid: Long?, tripTour: TripTour): TripTour {
        val userData = userDataDao.getUserByID(userid)
        val newtrip = TripTour()
        newtrip.tripName = tripTour.tripName
        newtrip.userData = userData
        return tripTourDao.save(newtrip)
    }

    override fun getTripTourById(tripid: Long?): TripTour {
        return tripTourDao.getTripTourById(tripid)
    }

    override fun getTriptour(): List<TripTour> {
        return tripTourDao.getTriptour()
    }

    @Autowired
    lateinit var tripTourDao: TripTourDao

    @Autowired
    lateinit var tripTourDataDao: TripTourDataDao

    @Autowired
    lateinit var userDataDao: UserDataDao
}