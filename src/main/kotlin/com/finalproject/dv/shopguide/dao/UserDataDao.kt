package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.UserData

interface UserDataDao {
    fun getUserData(): List<UserData>
    fun getUserEmail(useremail: String?): UserData
    fun save(userData: UserData): UserData
    fun getUserByID(userid: Long?): UserData

}