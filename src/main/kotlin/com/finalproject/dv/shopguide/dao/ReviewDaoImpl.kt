package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.Review
import com.finalproject.dv.shopguide.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ReviewDaoImpl : ReviewDao {
    override fun save(reviewData: Review): Review {
        return reviewRepository.save(reviewData)
    }

    override fun getReviewById(reviewid: Long?): Review {
        return reviewRepository.findByIdAndIsDeletedIsFalse(reviewid)
    }

    override fun getReview(): List<Review> {
        return reviewRepository.findByIsDeletedIsFalse()
//        return reviewRepository.findAll().filterIsInstance(Review::class.java)
    }

    @Autowired
    lateinit var reviewRepository: ReviewRepository
}