package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.TripTourData

interface TripTourDataDao {
    fun getTripTourData(): List<TripTourData>
    fun save(addtriptour: TripTourData): TripTourData
    fun getTripTourDataById(tripid: Long?): TripTourData
    fun getTripTourDataByTripTourId(tripid: Long?): List<TripTourData>

}