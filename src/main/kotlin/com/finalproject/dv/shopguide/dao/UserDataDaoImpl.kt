package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.UserData
import com.finalproject.dv.shopguide.repository.UserDataRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDataDaoImpl : UserDataDao {
    override fun getUserByID(userid: Long?): UserData {
        return userDataRepository.findUserDataById(userid)
    }

    override fun save(userData: UserData): UserData {
        return userDataRepository.save(userData)
    }

    override fun getUserEmail(useremail: String?): UserData {
        return userDataRepository.findByEmailContainingIgnoreCase(useremail)
    }

    override fun getUserData(): List<UserData> {
        return userDataRepository.findAll().filterIsInstance(UserData::class.java)
    }

    @Autowired
    lateinit var userDataRepository: UserDataRepository
}