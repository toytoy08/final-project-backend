package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.TripTour
import com.finalproject.dv.shopguide.repository.TripTourRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository

class TripTourDaoImpl : TripTourDao {

    override fun save(newtrip: TripTour): TripTour {
        return tripTourRepository.save(newtrip)
    }

    override fun getTripTourById(tripid: Long?): TripTour {
        return tripTourRepository.findByIdAndIsDeletedIsFalse(tripid)
    }

    override fun getTriptour(): List<TripTour> {
        return tripTourRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var tripTourRepository: TripTourRepository
}