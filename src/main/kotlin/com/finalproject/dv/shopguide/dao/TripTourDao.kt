package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.TripTour

interface TripTourDao {
    fun getTriptour(): List<TripTour>
    fun getTripTourById(tripid: Long?): TripTour
    fun save(newtrip: TripTour): TripTour

}