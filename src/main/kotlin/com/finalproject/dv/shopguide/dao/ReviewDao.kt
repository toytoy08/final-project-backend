package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.Review

interface ReviewDao {
    fun getReview(): List<Review>
    fun getReviewById(reviewid: Long?): Review
    fun save(reviewData: Review): Review

}