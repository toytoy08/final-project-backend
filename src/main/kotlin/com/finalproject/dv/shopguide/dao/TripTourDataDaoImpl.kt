package com.finalproject.dv.shopguide.dao

import com.finalproject.dv.shopguide.entity.TripTourData
import com.finalproject.dv.shopguide.repository.TripTourDataRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TripTourDataDaoImpl : TripTourDataDao {
    override fun getTripTourDataByTripTourId(tripid: Long?): List<TripTourData> {
        return tripTourDataRepository.findTripTourDataByTripTourIdAndIsDeletedIsFalse(tripid)
    }

    override fun getTripTourDataById(tripid: Long?): TripTourData {
        return tripTourDataRepository.findByIdAndIsDeletedIsFalse(tripid)
    }

    override fun save(addtriptour: TripTourData): TripTourData {
        return tripTourDataRepository.save(addtriptour)
    }

    override fun getTripTourData(): List<TripTourData> {
        return tripTourDataRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var tripTourDataRepository: TripTourDataRepository
}